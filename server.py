import yaml
import socket
import json
from argparse import ArgumentParser


def make_request(text):
    return {
        'data': text
    }


if __name__ == '__main__':
    config = {
        'host': 'localhost',
        'port': 8003,
        'buffersize': 1024
    }
    parser = ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=False, help="Sets config path")
    parser.add_argument('-ht', '--host', type=str, required=False, help="Set host")
    parser.add_argument('-p', '--port', type=str, required=False, help="Set port")
    args = parser.parse_args()

    if args.config:
        with open(args.config) as file:
            file_config = yaml.safe_load(file)
            config.update(file_config or {})

    host = args.host if args.host else config.get('host')
    port = args.port if args.port else config.get('port')
    buffersize = config.get('buffersize')
    sock = socket.socket()
    sock.bind((host, port))
    sock.listen(5)
    print(f"Starting on port {port}")

    while True:
        client, (client_host, client_port) = sock.accept()
        try:
            print(f"Client {client_host}:{client_port}")

            bytes_request = client.recv(buffersize)
            response = {
                'status': 200,
                'request': bytes_request.decode()
            }
            print(f"Request {bytes_request.decode()}")
            client.send(json.dumps(response).encode())
        except Exception as e:
            response = {
                'status': 500,
                'request': bytes_request.decode(),
                'error': e
            }
        finally:
            client.close()



