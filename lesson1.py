import requests

print('''
Каждое из слов «разработка», «сокет», «декоратор» представить в строковом формате и проверить
тип и содержание соответствующих переменных. Затем с помощью онлайн-конвертера преобразовать строковые
представление в формат Unicode и также проверить тип и содержимое переменных.
''')
for word in ["разработка", "сокет", "декоратор"]:
    print(f"type={type(word)}, content={word}, byte={word.encode()}")

print('''
Каждое из слов «class», «function», «method» записать в байтовом типе без преобразования в последовательность кодов
(не используя методы encode и decode) и определить тип, содержимое и длину соответствующих переменных.
''')
for word in [b"class", b"function", b"method"]:
    print(f"type={type(word)}, content={word.decode()}, len={len(word)}")

print('''
Определить, какие из слов «attribute», «класс», «функция», «type» невозможно записать в байтовом типе.
''')
for word in ["attribute", "класс", "функция", "type"]:
    try:
        eval(f"b'{word}'")
        print(f"{word} can be in byte format. ")
    except SyntaxError as err:
        print(f"{word} can't be in byte format. {err}")

print('''
Преобразовать слова «разработка», «администрирование», «protocol», «standard» из строкового представления в байтовое и 
выполнить обратное преобразование (используя методы encode и decode).
''')
for word in ["разработка", "администрирование", "protocol", "standard"]:
    print(f"string={word}, encoded_string={word.encode()}, encoded_and_decoded_string={word.encode().decode()}")

print('''
Выполнить пинг веб-ресурсов yandex.ru, youtube.com и преобразовать результаты из байтовового в строковый тип на кириллице.
''')

r = requests.get("https://youtube.ru")
print(r.encoding)
# работает из коробки, в result - строка в utf-8 с кирилицей
result = r.text

r = requests.get("https://yandex.ru")
print(r.encoding)
r.encoding="utf-8"
# работает из коробки, в result - строка в utf-8 с кирилицей
result = r.text

print('''
Создать текстовый файл test_file.txt, заполнить его тремя строками: «сетевое программирование», «сокет», «декоратор».
Проверить кодировку файла по умолчанию. Принудительно открыть файл в формате Unicode и вывести его содержимое.
''')
with open("test_file.txt", "r", encoding="utf-8") as f:
    print(f.read())




