import json
import yaml

with open("data/readme.json", "r") as jsonfile_read:
    data = json.load(jsonfile_read)
with open("data/writeme.yml", "w") as yml_writefile:
    yaml.dump(data, yml_writefile)
