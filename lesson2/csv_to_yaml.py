import csv
import yaml

with open("data/readme.csv", "r") as csvfile:
    reader = csv.DictReader(csvfile)
    data = {}
    for fieldname in reader.fieldnames:
        data[fieldname] = []

    for row in reader:
        for fieldname, value in row.items():
            data[fieldname].append(value)

with open("data/writeme.yml", "w") as yml_writefile:
    yaml.dump(data, yml_writefile)



