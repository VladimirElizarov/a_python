import yaml

yml_readfile = open("data/readme.yml", "r")
data = yaml.safe_load(yml_readfile)

data["a"] = "some value"

yml_writefile = open("data/writeme.yml", "w")
yaml.dump(data, yml_writefile)

