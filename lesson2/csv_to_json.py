import csv
import json

with open("data/readme.csv", "r") as csvfile:
    reader = csv.DictReader(csvfile)
    data = {}
    for fieldname in reader.fieldnames:
        data[fieldname] = []

    for row in reader:
        for fieldname, value in row.items():
            data[fieldname].append(value)

with open("data/writeme.json", "w") as jsonfile_write:
    json.dump(data, jsonfile_write, indent=4)


