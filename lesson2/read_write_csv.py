import csv

with open("data/readme.csv", "r") as csvfile_read:
    reader = csv.DictReader(csvfile_read)
    with open("data/writeme.csv", "w") as csvfile_write:
        writer = csv.DictWriter(csvfile_write, fieldnames=reader.fieldnames)
        writer.writeheader()
        for row in reader:
            row["head1"] = row["head1"] + "some value"
            writer.writerow(row)

