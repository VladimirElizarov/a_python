import json

with open("data/readme.json", "r") as jsonfile_read:
    data = json.load(jsonfile_read)
data["head1"] = "some new value"
with open("data/writeme.json", "w") as jsonfile_write:
    json.dump(data, jsonfile_write, indent=4)